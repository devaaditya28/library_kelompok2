require('dotenv').config() // kegunaan untuk membaca file .env
const express = require('express') //inisiasi variable yang berisi express
const port = process.env.PORT || 3500 // declare port dari .env || 3500
const app = express() // inisiasi fungsi express ke variable app
const cors = require('cors') // inisiasi variable yang berisi cors
const bodyParser = require('body-parser') // inisiasi variable yang berisi body-parser

app.use(cors()) // gunakan fungsi cors
app.use(express.urlencoded({ extended: true })) // berguna untuk menerima request form-data dan urlencode form
app.use(bodyParser.json()) // gunakan fungsi json dari body parser (berguna untuk menangkap json request)


app.listen(port, () => {
    console.log(`server is running at port ${port}`)
}) // fungsi untuk inisiasi http server pada port yang telah di tentukan